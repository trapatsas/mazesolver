﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheMazeSolver;
using TheMazeSolver.CustomException;
using TheMazeSolver.Validation;

namespace TestMazeSolver
{
    [TestClass]
    public class ExceptionsTest
    {

        [TestMethod]
        [ExpectedException(typeof(ImproperFileFormatException))]
        public void ConfirmImproperFileFormatException1()
        {
            Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Invalid\Case01.mzm"));
        }

        [TestMethod]
        [ExpectedException(typeof(ImproperFileFormatException))]
        public void ConfirmImproperFileFormatException2()
        {
            Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Invalid\Case02.mzm"));


        }

        [TestMethod]
        [ExpectedException(typeof(ImproperFileFormatException))]
        public void ConfirmImproperFileFormatException3()
        {
            Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Invalid\Case03.mzm"));

        }

        [TestMethod]
        [ExpectedException(typeof(ImproperFileFormatException))]
        public void ConfirmImproperFileFormatException4()
        {
            Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Invalid\Case04.mzm"));

        }

        [TestMethod]
        [ExpectedException(typeof(ImproperFileFormatException))]
        public void ConfirmImproperFileFormatException5()
        {
            Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Invalid\Case05.mzm"));

        }

        [TestMethod]
        [ExpectedException(typeof(ImproperFileFormatException))]
        public void ConfirmImproperFileFormatException6()
        {
            Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Invalid\Case06.mzm"));

        }

        [TestMethod]
        [ExpectedException(typeof(ImproperFileFormatException))]
        public void ConfirmImproperFileFormatException7()
        {
            Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Invalid\Case07.mzm"));

        }

        [TestMethod]
        [ExpectedException(typeof(ImproperFileFormatException))]
        public void ConfirmImproperFileFormatException8()
        {
            Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Invalid\Case08.mzm"));

        }

        [TestMethod]
        [ExpectedException(typeof(ImproperFileFormatException))]
        public void ConfirmImproperFileFormatException9()
        {
            Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Invalid\Case09.mzm"));

        }

        [TestMethod]
        [ExpectedException(typeof(ImproperFileFormatException))]
        public void ConfirmImproperFileFormatException10()
        {
            Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Invalid\Case10.mzm"));

        }

        [TestMethod]
        [ExpectedException(typeof(ImproperFileFormatException))]
        public void ConfirmImproperFileFormatException11()
        {
            Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Invalid\Case11.mzm"));

        }
    }
}
