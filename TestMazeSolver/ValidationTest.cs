﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheMazeSolver;
using TheMazeSolver.Validation;

namespace TestMazeSolver
{
    [TestClass]
    public class ValidationTest
    {
        [TestMethod]
        public void IsMazeFileValid01()
        {
            try
            {
                Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Case01.mzm"));

            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void IsMazeFileValid02()
        {
            try { Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Case02.mzm"));

            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void IsMazeFileValid03()
        {
            try { Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Case03.mzm"));

            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void IsMazeFileValid04()
        {
            try { Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Case04.mzm"));

            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void IsMazeFileValid05()
        {
            try { Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Case05.mzm"));

            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void IsMazeFileValid06()
        {
            try { Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Case06.mzm"));

            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void IsMazeFileValid07()
        {
            try { Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Case07.mzm"));

            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

        [TestMethod]
        public void IsMazeFileValid08()
        {
            try { Validation.ValidateMazeFile(File.ReadAllLines(@"..\..\Mazes\Case08.mzm"));

            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }
        }

    }
}
