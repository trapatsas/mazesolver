﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheMazeSolver;
using TheMazeSolver.Logic;
using TheMazeSolver.Model;

namespace TestMazeSolver
{
    [TestClass]
    public class IntegrationTest
    {

        [TestMethod]
        public void SearchMaze01()
        {
            IMaze theMaze = new Maze(File.ReadAllLines(@"..\..\Mazes\Case01.mzm"));

            var finder = new BreadthFirstSearch(theMaze);

            Assert.IsTrue(finder.Execute());

            var path = finder.GetResultStack().Count;

            Assert.AreEqual(13,path);

            var ops = finder.Operations;

            Assert.AreEqual(63, ops);

        }

        [TestMethod]
        public void SearchMaze02()
        {
            IMaze theMaze = new Maze(File.ReadAllLines(@"..\..\Mazes\Case02.mzm"));

            var finder = new BreadthFirstSearch(theMaze);

            Assert.IsTrue(finder.Execute());

            var path = finder.GetResultStack().Count;

            Assert.AreEqual(11, path);

            var ops = finder.Operations;

            Assert.AreEqual(46, ops);

        }

        [TestMethod]
        public void SearchMaze03()
        {
            IMaze theMaze = new Maze(File.ReadAllLines(@"..\..\Mazes\Case03.mzm"));

            var finder = new BreadthFirstSearch(theMaze);

            Assert.IsTrue(finder.Execute());

            var path = finder.GetResultStack().Count;

            Assert.AreEqual(0, path);

            var ops = finder.Operations;

            Assert.AreEqual(1, ops);

        }

        [TestMethod]
        public void SearchMaze08()
        {
            IMaze theMaze = new Maze(File.ReadAllLines(@"..\..\Mazes\Case08.mzm"));

            var finder = new BreadthFirstSearch(theMaze);

            Assert.IsTrue(finder.Execute());

            var path = finder.GetResultStack().Count;

            Assert.AreEqual(37, path);

            var ops = finder.Operations;

            Assert.AreEqual(261, ops);

        }

    }
}
