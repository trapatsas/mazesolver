﻿using System.Collections.Generic;
using System.Drawing;

namespace TheMazeSolver
{
    public static class Extension
    {
        public static List<Point> GetNeighbors(this Point p)
        {
            return new List<Point>()
            {
                new Point(p.X + 1, p.Y),
                new Point(p.X - 1, p.Y),
                new Point(p.X, p.Y + 1),
                new Point(p.X, p.Y - 1)
            };
        }
    } 
}
