﻿using System;
using System.Diagnostics;
using System.Reflection;
using log4net;
using log4net.Config;
using TheMazeSolver.UI;

[assembly: XmlConfigurator(Watch = true)]

namespace TheMazeSolver
{
    public static class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void Main(string[] args)
        {
            Log.Info("Application initialized.");

            if (Debugger.IsAttached)
            {
                Log.Warn("**You are debugging**");
                Console.ReadKey();
                Console.Clear();
                MazeSolver.StartApplication(@"Case07.mzm");
            }
            else
            {

                /*
                 * Check if user provided valid arguments
                 */
                if (args.Length == 1 && Help.HelpRequired(args[0]))
                {
                    Help.DisplayHelp();
                }
                else if (args.Length == 1 && Validation.Validation.ValidateUserInput(args[0]))
                {
                    Log.Info("Input validation passed.");
                    MazeSolver.StartApplication(args[0]);
                }
                else
                {
                    Help.DisplayInfo();
                }
                Log.Info("Exit");
            }
        }
    }
}
