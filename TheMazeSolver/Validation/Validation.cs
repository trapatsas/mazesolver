﻿using System.IO;
using TheMazeSolver.CustomException;
using TheMazeSolver.Properties;

namespace TheMazeSolver.Validation
{
    public static class Validation
    {
        public static bool ValidateUserInput(string path)
        {
            return (File.Exists(path) && path.EndsWith(Resources.MZM_EXTENSION));
        }

        public static void ValidateMazeFile(string[] file)
        {
            var countStart = 0;

            var countGoal = 0;

            var mazeHeight = file.Length;

            if (mazeHeight == 0) throw new ImproperFileFormatException(Resources.IMPROPERLY_FORMATTED_FILE);

            var mazeWidth = file[0].Length;

            var isValidMzm = (mazeHeight + mazeWidth > 2);

            foreach (var line in file)
            {
                isValidMzm &= (line.Length == mazeWidth);

                foreach (var ch in line)
                {
                    countStart += (ch == Settings.Default.Start) ? 1 : 0;
                    countGoal += (ch == Settings.Default.Goal) ? 1 : 0;
                }
            }

            isValidMzm &= (countStart == 1 && countGoal == 1);

            if (!isValidMzm) throw new ImproperFileFormatException(Resources.IMPROPERLY_FORMATTED_FILE);
        }
    }
}
