﻿using System;
using System.Reflection;
using log4net;
using TheMazeSolver.Properties;

namespace TheMazeSolver.UI
{
    internal static class Help
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        internal static void DisplayInfo()
        {
            Console.WriteLine(Resources.DISPLAY_INFO);
            Log.Info("User requested info");
        }

        internal static bool HelpRequired(string param)
        {
            return param == "-h" || param == "--help" || param == "/?";
        }

        internal static void DisplayHelp()
        {
            Console.WriteLine();
            Console.WriteLine(Resources.USAGE);
            Console.WriteLine();
            Console.WriteLine(Resources.OPTIONS);
            Log.Info("User requested help");
        }
    }
}
