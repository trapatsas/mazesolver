﻿using System;
using TheMazeSolver.Properties;

namespace TheMazeSolver.UI
{
    public static class Banner
    {
        public static UserChoice Show()
        {
            var arr = new[]
            {
                @"      ╔══════════════════════════════════╗      ",
                @"      ║                                  ║      ",
                @"      ║   Wellcome to The Maze Solver!   ║      ",
                @"      ║       Your file is valid.        ║      ",
                @"      ║                                  ║      ",
                @"      ║   Choose:                        ║      ",
                @"      ║   B - for Breadth First Search,  ║      ",
                //@"      ║   D - for Depth First Search or  ║      ",
                @"      ║   Q - to Quit                    ║      ",
                @"      ╚══════════════════════════════════╝      "
            };

            foreach (var line in arr)
                Console.WriteLine(line);

            Console.WriteLine();

            UserChoice uc = UserChoice.NotSet;

            while (uc == UserChoice.NotSet)
            {
                var input = GetUserInput();
                Console.WriteLine();

                switch (input)
                {
                    case 'b':
                    case 'B':
                        uc = UserChoice.Bfs;
                        break;
                    case 'q':
                    case 'Q':
                        uc = UserChoice.Quit;
                        break;
                }

                if(uc == UserChoice.NotSet)
                    Console.WriteLine(Resources.MAKE_VALID_CHOICE);
            }

            Console.Clear();
            return uc;
        }

        private static char GetUserInput()
        {
            char input = Console.ReadKey().KeyChar;
            return input;
        }
    }
}
