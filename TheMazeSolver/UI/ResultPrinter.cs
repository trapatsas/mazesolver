﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using TheMazeSolver.Model;
using TheMazeSolver.Properties;

namespace TheMazeSolver.UI
{
    public static class ResultPrinter
    {
        public static void PrintSolutionPath(Stack<Point> resultStack)
        {
            var cursorHeight = Console.CursorTop;

            if (resultStack.Count > 0)
            {
                foreach (Point p in resultStack)
                {
                    Console.SetCursorPosition(p.X, p.Y);
                    Thread.Sleep(Settings.Default.SleepTime*2);
                    PrettyPrint(ConsoleColor.Yellow, Settings.Default.Star);
                }
                Console.SetCursorPosition(0, 0);
                Console.CursorTop = cursorHeight + 1;
                Console.WriteLine(Resources.SOLUTION_IS);


                foreach (Point p in resultStack)
                {
                    PrettyPrint(ConsoleColor.Cyan, string.Format("({0}, {1}),", p.X, p.Y));
                }
            }
            else
            {
                Console.WriteLine();
                PrettyPrint(ConsoleColor.Red, Resources.PATH_TOO_SHORT);
            }

        }

        public static void DrawMaze(IMaze m)
        {
            //Console.Clear();

            const string charSprites = " ║═╝";

            char c;

            for (int i = 0; i < m.Height; i++)
            {
                for (int j = 0; j < m.Width; j++)
                {
                    Thread.Sleep(Settings.Default.SleepTime);
                    Point p = new Point(j, i);
                    if (p == m.StartingPoint)
                    {
                        PrettyPrint(ConsoleColor.Green, Settings.Default.Start);
                    }
                    else if (p == m.GoalPoint)
                    {
                        PrettyPrint(ConsoleColor.Red, Settings.Default.Goal);
                    }
                    else if (!m.IsEmptySpace(p))
                    {
                        PrettyPrint(ConsoleColor.White, Settings.Default.Wall);
                    }
                    else
                    {
                        PrettyPrint(ConsoleColor.Gray, Settings.Default.EmptySpace);
                    }
                }
                c = charSprites[1];
                Console.Write(c);
                Console.WriteLine();
            }
            for (int i = 0; i < m.Width; i++)
            {
                c = charSprites[2];
                Console.Write(c);
            }
            c = charSprites[3];
            Console.Write(c);

        }

        public static void PrettyPrint(ConsoleColor a, char b)
        {
            char c;
            Console.ForegroundColor = a;
            c = b;
            Console.Write(c);
            Console.ResetColor();
        }

        public static void PrettyPrint(ConsoleColor a, string b)
        {
            string c;
            Console.ForegroundColor = a;
            c = b;
            Console.Write(c);
            Console.ResetColor();
        }
    }
}
