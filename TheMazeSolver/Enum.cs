﻿namespace TheMazeSolver
{
    public enum BlockType
    {
        Empty,
        Goal,
        Start,
        Wall
    };

    public enum UserChoice
    {
        NotSet,
        Bfs,
        Dfs,
        Quit
    };
}
