﻿using System;
using System.IO;
using System.Reflection;
using log4net;
using TheMazeSolver.CustomException;
using TheMazeSolver.Logic;
using TheMazeSolver.Model;
using TheMazeSolver.Properties;
using TheMazeSolver.UI;

namespace TheMazeSolver
{
    public static class MazeSolver
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static void StartApplication(string filePath)
        {
            try
            {
                /*
                 *  1. Read the file
                 */
                var mazeMapFile = File.ReadAllLines(filePath);


                /*
                 *  2. If maze file in improper format log & exit
                 */
                Validation.Validation.ValidateMazeFile(mazeMapFile);


                Log.Info(Resources.MZM_FILE_OK);

                /*
                 *  3. Build the maze...
                 */
                IMaze theMaze = new Maze(mazeMapFile);

                /*
                 *  4. Select algorithm
                 */
                UserChoice choice = Banner.Show();

                /*
                 *  5. Initialize your search algorithm
                 */
                IPathfinder search = InitSearchByUserChoice(choice);
                search.TheMaze = theMaze;

                /*
                 *  6. Execute the algorithm & return the results
                 */
                if (search.Execute())
                {
                    var resultStack = search.GetResultStack();

                    ResultPrinter.DrawMaze(theMaze);

                    ResultPrinter.PrintSolutionPath(resultStack);
                }
                else
                    ResultPrinter.PrettyPrint(ConsoleColor.Red, Resources.NO_SOLUTION_FOUND);

                /*
                 *  7. Nice. Bye!
                 */
                Log.Info(Resources.EXIT_ON_SUCCESS);
            }
            catch (ImproperFileFormatException fex)
            {
                Log.Error(fex);
            }
            catch (IOException ioex)
            {
                Log.Error("An IO error occurred: '{0}'", ioex);
            }
            catch (Exception ex)
            {
                // Uh oh something else went wrong
                Log.Error("{0}", ex);
            }
        }

        private static IPathfinder InitSearchByUserChoice(UserChoice choice)
        {
            IPathfinder pf = null;
            switch (choice)
            {
                case UserChoice.Bfs:
                    pf = new BreadthFirstSearch();
                    break;
                //case UserChoice.Dfs:
                    //pf = new BreadthFirstSearch(); //TODO Implement the DFS algorithm
                    //break;
                case UserChoice.Quit:
                    Environment.Exit(0);
                    break;
                default:
                    pf = new BreadthFirstSearch();
                    break;

            }
            return pf;
        }
    }
}
