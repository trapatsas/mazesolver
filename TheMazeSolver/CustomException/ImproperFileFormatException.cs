﻿using System;

namespace TheMazeSolver.CustomException
{
    public class ImproperFileFormatException : Exception
    {
        public ImproperFileFormatException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public ImproperFileFormatException(string format)
            : base(string.Format(format)) { }
    }
}
