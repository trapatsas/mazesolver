﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using log4net;
using TheMazeSolver.Properties;

namespace TheMazeSolver.Model
{
    public class Maze : IMaze
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public BlockType[,] MazeMatrix { get; private set; }
        public Point StartingPoint { get; private set; }
        public Point GoalPoint { get; private set; }
        public int Height { get; private set; }
        public int Width { get; private set; }
        public int Offset { get; private set; }

        private Maze()
        {
        }

        public Maze(string[] matrix)
        {
            Offset = Settings.Default.MazeOffset;
            Height = matrix.Length;
            Width = matrix[0].Length;
            MazeMatrix = Build(matrix, matrix[0].Length, matrix.Length);
            StartingPoint = FindPointByType(BlockType.Start);
            GoalPoint = FindPointByType(BlockType.Goal);
            Log.Info("Maze created!");
        }

        /// <summary>
        /// Builds a 2-D array of BlockTypes from a string array with known dimensions
        /// </summary>
        /// <param name="matrix">The string array.</param>
        /// <param name="mazeWidth">Array Width.</param>
        /// <param name="mazeHeight">Array Height.</param>
        /// <returns>Returns 2-D BlockType array.</returns>
        public static BlockType[,] Build(string[] matrix, int mazeWidth, int mazeHeight)
        {
            var mzm = new BlockType[mazeWidth, mazeHeight];

            var lineNum = 0;

            foreach (var line in matrix)
            {
                var lineLength = line.Length;

                for (var index = 0; index < lineLength; index++)
                {
                    mzm[index, lineNum] = GetBlockType(line[index]);
                }
                lineNum++;
            }
            return mzm;
        }

        /// <summary>
        /// Converts a char to its corresponding block type
        /// </summary>
        /// <param name="singleChar">A single char.</param>
        /// <returns></returns>
        private static BlockType GetBlockType(char singleChar)
        {
            BlockType content;

            if (singleChar == Settings.Default.Wall)
                content = BlockType.Wall;
            else if (singleChar == Settings.Default.Start)
                content = BlockType.Start;
            else if (singleChar == Settings.Default.Goal)
                content = BlockType.Goal;
            else
                content = BlockType.Empty;

            return content;
        }

        /// <summary>
        /// Finds the first point that has the given block type. 
        /// Used for start and goal block types.
        /// </summary>
        /// <param name="blockType"></param>
        /// <returns></returns>
        public Point FindPointByType(BlockType blockType)
        {
            foreach (var point in AllBlocks(Width, Height).Where(point => MazeMatrix[point.X, point.Y] == blockType))
            {
                return new Point(point.X, point.Y);
            }
            return new Point(-1, -1);
        }

        private static IEnumerable<Point> AllBlocks(int mazeWidth, int mazeHeight)
        {
            /*
             * 
             * Return every point on the board in order.
             * 
             * */
            for (int x = 0; x < mazeWidth; x++)
            {
                for (var y = 0; y < mazeHeight; y++)
                {
                    yield return new Point(x, y);
                }
            }
        }

        /// <summary>
        /// Checks if the point is an unknown space or a wall
        /// </summary>
        /// <param name="current"></param>
        /// <returns></returns>
        public bool IsEmptySpace(Point current)
        {
            if (current.X >= Width || current.Y >= Height || current.X < Offset || current.Y < Offset) return false;
            var b = MazeMatrix[current.X, current.Y];
            return (b == BlockType.Empty || b == BlockType.Goal);
        }
    }
}
