﻿using System.Drawing;

namespace TheMazeSolver.Model
{
    public interface IMaze
    {
        Point StartingPoint { get; }
        Point GoalPoint { get; }
        int Height { get; }
        int Width { get; }

        /// <summary>
        /// Checks if the point is an unknown space or a wall
        /// </summary>
        /// <param name="current"></param>
        /// <returns></returns>
        bool IsEmptySpace(Point current);
    }
}
