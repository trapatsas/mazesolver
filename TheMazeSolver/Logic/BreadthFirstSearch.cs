﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using log4net;
using TheMazeSolver.Model;

namespace TheMazeSolver.Logic
{
    public class BreadthFirstSearch : IPathfinder
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public Dictionary<Point, Point> ConnectedTo { get; set; }
        public Stack<Point> ResultStack { get; set; }
        public bool GoalFound { get; set; }
        public int Operations { get; set; }
        public IMaze TheMaze { get; set; }

        public BreadthFirstSearch()
        {
            ConnectedTo = new Dictionary<Point, Point>();
            ResultStack = new Stack<Point>();
        }

        public BreadthFirstSearch(IMaze maze)
        {
            ConnectedTo = new Dictionary<Point, Point>();
            ResultStack = new Stack<Point>();
            TheMaze = maze;
        }

        /// <summary>
        /// Implementation of the Breadth First Search algorithm. Returns true on successfull execution.
        /// </summary>
        /// <returns></returns>
        public bool Execute()
        {
            Dictionary<Point, bool> visited = new Dictionary<Point, bool>();

            /*
            * 2-D matrix is really a graph...
            * https://en.wikipedia.org/wiki/Adjacency_matrix
            * so, we can do:
            */
            Queue<Point> q = new Queue<Point>();
            visited.Add(TheMaze.StartingPoint, false);
            q.Enqueue(TheMaze.StartingPoint);
            while (q.Count > 0 && !GoalFound)
            {
                Point current = q.Dequeue();
                foreach (var neighbor in current.GetNeighbors().Where(neighbor => TheMaze.IsEmptySpace(neighbor) && (!visited.ContainsKey(neighbor) || !visited[neighbor])))
                {
                    Operations++;

                    ConnectedTo.Add(neighbor, current);

                    if (neighbor == TheMaze.GoalPoint)
                    {
                        Log.Info(string.Format("Found Goal Point: {0}, {1}", neighbor.X + 1, neighbor.Y + 1));
                        GoalFound = true;
                        break;
                    }

                    if (!visited.ContainsKey(neighbor)) visited.Add(neighbor, true);
                    q.Enqueue(neighbor);
                }
            }
            Log.Info(string.Format("Count of operations: '{0}'", Operations));
            return GoalFound;
        }

        /// <summary>
        /// Returns a stack with the path from the Start to the Goal Point
        /// </summary>
        /// <returns></returns>
        public Stack<Point> GetResultStack()
        {
            var currentPoint = ConnectedTo[TheMaze.GoalPoint];

            while (!Equals(TheMaze.StartingPoint, currentPoint))
            {
                ResultStack.Push(currentPoint);
                currentPoint = ConnectedTo[currentPoint];
                Log.Info(string.Format("X = {0}, Y = {1}", currentPoint.X + 1, currentPoint.Y + 1));
            }
            return ResultStack;
        }
    }
}
