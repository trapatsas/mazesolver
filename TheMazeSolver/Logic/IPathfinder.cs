﻿using System.Collections.Generic;
using System.Drawing;
using TheMazeSolver.Model;

namespace TheMazeSolver.Logic
{
    public interface IPathfinder
    {
            Dictionary<Point, Point> ConnectedTo { get; set; }
            Stack<Point> ResultStack { get; set; }
            bool GoalFound { get; set; }
            int Operations { get; set; }
            IMaze TheMaze { get; set; }
            bool Execute();
            Stack<Point> GetResultStack();
    }
}